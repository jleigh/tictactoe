class WinChecker

  def self.winner?(last_x, last_y, current_player, grid_size)
    moves = current_player.moves
    column = moves[last_x.to_s]
    row = moves.keys.map { |key| moves[key][last_y.to_s] }.compact
    diagonals = find_diagonal(moves, grid_size) if moves.keys.length == grid_size
    win?(column, row, diagonals, grid_size)
  end

  def self.win?(column, row, diagonals, grid_size)
    (column.keys.length == grid_size || row.length == grid_size) || diagonals?(diagonals, grid_size)
  end

  def self.diagonals?(diagonals, grid_size)
    if diagonals
      diagonals.dig(:left_diagonal).length == grid_size || 
      diagonals.dig(:right_diagonal).length == grid_size
    else
      false
    end
  end

  def self.find_diagonal(moves, grid_size)
    left_diagonal = moves.keys.map { |key| moves[key][key] }.compact
    right_diagonal = moves.keys.map do |key|
      x = key.to_i
      y = grid_size - x - 1
      moves[key][y.to_s]
    end.compact

    {left_diagonal: left_diagonal, right_diagonal: right_diagonal}
  end
end