class Player
  attr_reader :character
  attr_accessor :moves

  def initialize(character)
    @character = character
    @moves = {}
  end
end