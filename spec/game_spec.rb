require 'spec_helper'

describe Game do
  let(:player_x) { Player.new('x') }
  let(:player_o) { Player.new('o') }
  let(:win_checker_class) { WinChecker }
  let!(:grid_size) { 3 }

  subject(:game) { Game.new(player_x, player_o, grid_size, win_checker_class) }
  describe '#initialize' do
    it 'initializes with correct values' do
      expect(game.player_o.character).to eq('o')
      expect(game.player_x.character).to eq('x')
      expect(game.current_player.character).to eq('x')
      expect(game.win_checker).to eq(WinChecker)
      expect(game.grid_size).to eq(3)
      expect(game.winner).to eq(nil)
      expect(game.turn).to eq(1)
    end
  end

  describe '#play' do
    context 'when move is within boundaries' do
      it 'increments turn, changes player and updates the correct moves' do
        game.play(0,1)
        expect(game.turn).to eq(2)
        expect(game.current_player.character).to eq('o')
        expect(game.player_x.moves).to eq({"0" => {"1" => true}})
      end

      it 'increments turn, changes player and updates the correct moves' do
        game.play(0,1)
        game.play(1,2)
        expect(game.turn).to eq(3)
        expect(game.current_player.character).to eq('x')
        expect(game.player_x.moves).to eq({"0" => {"1" => true}})
        expect(game.player_o.moves).to eq({"1" => {"2" => true}})
      end
    end

    context 'when move is not within boundaries' do
      let!(:invalid_coordinate) { grid_size + 1 }
      it 'raises an error' do
        expect{ game.play(0, invalid_coordinate) }.to raise_error "InvalidMove: The given move is out of bounds." 
        expect{ game.play(invalid_coordinate, 1) }.to raise_error "InvalidMove: The given move is out of bounds." 
      end
    end

    context 'when move is already taken' do
      it 'raises an error' do
        game.play(0,1)
        expect{ game.play(0,1) }.to raise_error "MoveTaken: The given move is already taken." 
      end
    end

    context 'when there is a winner or game is over' do
      it 'raises an error' do
        expect(game).to receive(:winner).and_return('x')
        expect{ game.play(0,1) }.to raise_error "GameOver: No more moves." 
      end
    end
  end

  describe '#check_for_win' do
    context 'when turn is >= minimum turn' do
      context 'when there is a winner' do
        it 'calls win checker' do
          expect(WinChecker).to receive(:winner?).and_call_original
          game.play(0,0)
          game.play(1,2)
          game.play(0,1)
          game.play(2,0)
          game.play(0,2)
          expect(game.winner.character).to eq('x')
        end
      end
    end

    context 'when turn is < 5' do
      it 'does nothing' do
        expect(game).to receive(:turn).and_return(3)
        expect(WinChecker).to_not receive(:winner?)
        game.check_for_win(2,0)

        expect(game.winner).to eq(nil)
      end
    end
  end
end