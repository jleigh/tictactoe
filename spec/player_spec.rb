require 'spec_helper'

describe Player do
  subject(:player) { Player.new('x') }
  describe '#initialize' do
    it 'initializes with the correct values' do
      expect(player.character).to eq('x')
      expect(player.moves).to eq({})
    end
  end
end