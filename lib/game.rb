class Game
  attr_reader :player_x, :player_o, :winner, :turn, :grid_size, :current_player, :win_checker

  def initialize(player_x, player_o, grid_size, win_checker)
    @player_x, @player_o = player_x, player_o
    @current_player = @player_x
    @win_checker = win_checker
    @grid_size = 3
    @winner = nil
    @turn = 1
  end

  def play(x, y)
    check_for_errors(x, y)
    update_moves(x, y)
    check_for_win(x, y)
    keep_playing if !@winner
  end

  def check_for_win(x, y)
    return if turn < minimum_turn
    if win_checker.winner?(x, y, current_player, grid_size)
      @winner = current_player
    end
  end

  def keep_playing
    increment_turn
    change_player
  end

  private
  def minimum_turn
    (2 * grid_size - 1)
  end

  def update_moves(x, y)
    character = current_player.character
    moves = public_send("player_#{character}").moves

    if moves[x.to_s]
      moves[x.to_s][y.to_s] = true
    else
      moves[x.to_s] = {y.to_s => true}
    end

    public_send("player_#{character}").moves = moves
  end

  def increment_turn
    @turn = turn + 1
  end

  def change_player
    if current_player.character == 'x'
      @current_player = @player_o
    else
      @current_player = @player_x
    end
  end

  def invalid_move?(x, y)
    (x > grid_size || y > grid_size) || (x < 0 || y < 0)
  end

  def move_taken?(x, y)
    player_x.moves.dig(x.to_s, y.to_s) || player_o.moves.dig(x.to_s, y.to_s)
  end

  def check_for_errors(x, y)
    raise "GameOver: No more moves." if winner
    raise "InvalidMove: The given move is out of bounds."  if invalid_move?(x, y)
    raise "MoveTaken: The given move is already taken."  if move_taken?(x, y)
  end
end