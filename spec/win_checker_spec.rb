require 'spec_helper'

describe 'WinChecker' do
  let(:player) { Player.new('x') }
  let(:grid_size) { 3 }
  describe '.winner?' do
    context 'when there is no win' do
      it 'returns false' do
          columnar_win = {"0" => {"0" => true, "1" => true}, "2" => {"0" =>true}}
          expect(player).to receive(:moves).and_return(columnar_win)
          response = WinChecker.winner?(2, 0, player, grid_size)
          expect(response).to eq(false)
        end
    end

    context 'when there is a win' do
      context 'horizontal win' do
        it 'returns true' do
          columnar_win = {"0" => {"0" => true, "1" => true, "2" => true}}
          expect(player).to receive(:moves).and_return(columnar_win)
          response = WinChecker.winner?(0, 2, player, grid_size)
          expect(response).to eq(true)
        end
      end

      context 'vertical win' do
        it 'returns true' do
          row_win = {"1" => {"0" => true}, "2" => {"0" => true}, "0" => {"0" => true}}
          expect(player).to receive(:moves).and_return(row_win)
          response = WinChecker.winner?(1, 0, player, grid_size)
          expect(response).to eq(true)
        end
      end

      context 'left diagonal win' do
        it 'returns true' do
          left_diagonal_win = {"0" => {"0" => true}, "2" => {"2" => true}, "1" => {"1" => true}}
          expect(player).to receive(:moves).and_return(left_diagonal_win)
          response = WinChecker.winner?(2, 2, player, grid_size)
          expect(response).to eq(true)
        end
      end

      context 'right diagonal win' do
        it 'returns true' do
          right_diagonal_win = {"2" => {"0" => true}, "1" => {"1" => true}, "0" => {"2" => true}}
          expect(player).to receive(:moves).and_return(right_diagonal_win )
          response = WinChecker.winner?(2, 0, player, grid_size)
          expect(response).to eq(true)
        end
      end
    end
  end
end